﻿using System;
using System.Collections.Generic;

namespace IMC
{
    class Program
    {
        private static int nCandidatos;
        private static Comitiva comitiva = new();
        
        static void Main(string[] args)
        {
            
            Console.Write("Quantos atletas serão cadastrados? ");
            nCandidatos = int.Parse(Console.ReadLine());

            for (int i = 0; i < nCandidatos; i++)
            {
               
                Console.Write("\nDigite o nome: ");
                string nome = Console.ReadLine();

                Console.Write("Digite o peso do Atleta: ");
                double peso = double.Parse(Console.ReadLine());

                Console.Write("Digite a altura do Atleta: ");
                double altura = double.Parse(Console.ReadLine());

                Atleta atleta = new(nome, peso, altura);
                
                var imc = comitiva.Calc(atleta.Peso, atleta.Altura);
                comitiva.VerificaImc(imc, atleta);
            }

            if (comitiva.Atletas.Count > 0)
            {
                Console.WriteLine("\nComitiva:");
                foreach (var atleta in comitiva.Atletas)
                {
                    Console.WriteLine($"{atleta}");
                }
            }
            else
            {
                Console.WriteLine("Comitiva não possui atletas");
            }
        }
    }
}
