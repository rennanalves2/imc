﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMC
{
    class Atleta
    {    
        public string Nome { get; set; }
        public double Peso { get; set; }
        public double Altura { get; set; }

        public Atleta(string nome, double peso, double altura)
        {
            Nome = nome;
            Peso = peso;
            Altura = altura;
        }

        public override string ToString()
        {
            return $"Nome: {Nome} \nPeso: {Peso} \nAltura: {Altura}";
        }
    }
}
