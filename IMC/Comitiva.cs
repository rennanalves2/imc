﻿using System;
using System.Collections.Generic;

namespace IMC
{
    class Comitiva
    {
        public List<Atleta> Atletas = new List<Atleta>();
        
        public double imc { get; set; }

        public double Calc(double Peso, double Altura)
        {
            double alt = Math.Pow(Altura / 100, 2);
            double imc = Peso / alt;
            return imc;
        }

        public void VerificaImc(double imc, Atleta atleta)
        {
            if (imc >18.5 && imc<25)
            {
                Atletas.Add(atleta);
            }
        }
    }
}
